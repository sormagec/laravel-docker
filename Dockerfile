FROM php:7.2-alpine

# Install dev dependencies
RUN apk add --no-cache --virtual .build-deps \
    $PHPIZE_DEPS \
    curl-dev \
    imagemagick-dev \
    libtool \
    libxml2-dev \
    postgresql-dev \
    sqlite-dev

# Install production dependencies
RUN apk add --no-cache \
    bash \
    curl \
    g++ \
    gcc \
    git \
    imagemagick \
    libc-dev \
    libpng-dev \
    make \
    mysql-client \
    nodejs \
    nodejs-npm \
    openssh-client \
    postgresql-libs \
    rsync

# Install PECL and PEAR extensions
RUN pecl install \
    imagick \
    xdebug \
    exif
RUN pear install PHP_CodeSniffer

# Install and enable php extensions
RUN docker-php-ext-enable \
    imagick \
    xdebug
RUN docker-php-ext-install \
    pdo_mysql \
    pdo_pgsql \
    pcntl \
    gd \
    zip \
    bcmath \
    exif
    
RUN pecl install mongodb redis \
    && docker-php-ext-enable mongodb redis

# Install composer
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="./vendor/bin:$PATH"

RUN composer global require "hirak/prestissimo"
RUN composer global require "laravel/envoy"

# Cleanup dev dependencies
RUN apk del -f .build-deps

# Setup working directory
WORKDIR /var/www

#RUN mkdir -p /data/db
#CMD [ "mongod", "--bind_ip", "0.0.0.0" ]

